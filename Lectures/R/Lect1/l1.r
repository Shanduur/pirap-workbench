rm(list = ls())

U <- c(c(1, 2), c(3, 4))
V <- c(1, 2, 3, 4)

X <- c("abc", "pqr", "abba")
Y <- c(123, "xyz", TRUE)
XY <- c(X, Y)

a <- V[1]
V[3] <- 10
V[10] <- 15
is.na(V[5])
length(V)
length(V) <- 100
X[length(X)] <- 12

W <- V[c(1, 3)]
Z <- c(1L, 2L)
Z[2] <- "qwerty"

# calculate 5*10^6 elements of logistic map

# variant 1
n = 5e6
r = 1.5
x1 <- 0.5

system.time({
    X = x1
    for(i in 2:n){
        X[i] <- r*X[i-1]*(1-X[i-1])
    }
})

# with preallocaion
system.time({
    Y = numeric(n)
    Y[1] <- x1
    for (i in 2:n) {
        Y[i] <- r*Y[i-1]*(1-Y[i-1])
    }
})

# fewer memory accesses
system.time({
    Z = numeric(n)
    Z[1] <- zi <- x1
    for (i in 2:n) {
        zi = r*zi*(1-zi)
        Z[i] <- zi
    }
})

# logical

I <- (X==Y)
J <- (X==Z)

all(I)
all(J)
any(I)
I[1] <- FALSE

rm(list=ls())
V <- c(1, 2, 3)
2 %in% V

c(3,5) %in% V

names(V) <- c("A", "B", "C")

X <- V["B"]
Y <- V[["B"]]

coeffs <- c(1, 2, 5, 10, 20, 50)
sizes <- 10^5*coeffs
subsetSize <- 10^4
keys = as.character(1:subsetSize)

times <- numeric()
for (i in 1:length(sizes)) {
   V = 1:sizes[i]
   names(V) <- V
   X <- sample(V)
    t0 <- Sys.time()
    subV <- V[keys]
    t1 <- Sys.time()
    times[i] <- as.numeric(t1-t0, units="secs")
}
# plot example
dev.new(width=5, height=4, unit="in")
plot(log(sizes), log(times), 
    type = "o",
    col="red",
    xaxt="n", yaxt="n",
    xlab = expression(paste("array size ", italic(N))),
    ylab = expression(paste(delta, italic(t), " [s]")),
)

# format X
axis(side=1, at=log(size), labels = F)
xlabels = parse(text=paste(coeffs, "%*% 10^5 ", sep=""))
text(
    x=log(sizes),
    y=par("usr")[3] - 0.1,
    labels = xlabels,
    srt = 45,
    pos=1,
    xpd=TRUE
)

pdf("chart.pdf", width=5, height=4)
plot(log(sizes), log(times))
dev.off()

# logical operators

A = c(T, F)
B = c(T, T)
C = c(FALSE, FALSE)

A & B
A && B
A && C

# Matrix

A <- matrix(data = c(1,2,3,4), nrow=2, ncol=2)
B <- matrix(data = c(1,2,3,4), nrow=2, ncol=2, byrow=T)

D <- matrix(data=c(1,2), nrow=1, ncol=2)
E <- matrix(data=c(1,2), nrow=2, ncol=1)

C <- array(data=seq(1,8), dim=c(2,2,2))

U <- matrix(nrow=2, ncol=3)
Z <- matrix(data=0, nrow=2, ncol=2)
O <- array(data=1, dim=c(1,2,3))
I <- diag(3)

m=3
n=5

U = matrix(data=runif(m*n, 100, 200), nrow=m, ncol=n)
N = matrix(data=rnorm(m*n, 2, 10), nrow=m, ncol=n)

A <- matrix(data = seq(1,6), nrow = 3, ncol = 2, byrow = T)
A
A[1, 1]

A[1, 2] <- 99

A[5, 1] <- 0

A[,1]
A[-1,]

B = A
B[1,1] <- 7

rowA <- A[3,]
colA <- A[,2]

