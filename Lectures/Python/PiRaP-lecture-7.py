import numpy as np

########################################################################################################################
# Paths

import os

wd = os.getcwd() 					# current working directory
f1 = os.path.exists('c:/abc.txt') 	# does the entry exist?
f2 = os.path.isdir('c:/abc.txt') 	# is it a directory?
f3 = os.path.isfile('c:/abc.txt') 	# is it a file?

dir = 'c:/Windows/'
for file in os.listdir(dir): 			# iterate over entries in a directory
	fullPath = os.path.join(dir, file)	# construct full path
	if os.path.isfile(fullPath):		# check if file
		(head, tail) = os.path.split(fullPath) 		# extract last component
		(file_wo_ext, ext) = os.path.splitext(tail)	# extract extension
		print(head, file_wo_ext, ext)


########################################################################################################################
# External processes

import subprocess

# run and wait
subprocess.call('dir /A:-D', shell=True)  # execute shell command - list all files

# I/O redirection:
# errors to file
# output to PIPE
logfile = open('errors.txt', "w")
cmd = ['dir', '/A:-D']
proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=logfile, shell=True)

proc.wait() 				# wait for finish
out = proc.stdout.read()	# read stdout as byte array
str = out.decode('utf-8')	# convert byte array to string
print(str)

# try incorrect invocation
cmd = ['dir', '--bla-bla', ]
proc = subprocess.Popen(cmd, stderr=logfile, stdout=subprocess.PIPE, shell=True)
proc.wait() 				# wait for finish

########################################################################################################################
# Serialization

import json

# dump to file
X = ['a', 2, [{1:True, 2:False}, 4.65]]
f = open('dump.json', 'w')
json.dump(X, f)
f.close()

# restore from file
f = open('dump.json', 'r')
Y = json.load(f)
f.close()

# more complex structures
class A:
	def __init__(self):
		self.X = []

# serialize class instance
a = A()
a.X = [1, 2, 3]
f = open('dump.json', 'w')
#json.dump(a, f) 			# error
json.dump(a.__dict__, f)	# simplest workaround - use internal dictionary that stores fields
f.close()

f = open('dump.json', 'r')
json_a = A()
json_a.__dict__ = json.load(f)
f.close()

# class instance inside collection
L = [A(), A()]
L[0].X = [5, 6, 7]

f = open('dump.json', 'w')
#json.dump(L, f) 			# error

# make customized encoder
class MyEncoder(json.JSONEncoder):
        def default(self, o):
            return o.__dict__

f.close()
f = open('dump.json', 'w')
json.dump(L, f, cls=MyEncoder)
f.close()

# decoding - see documentation...


########################################################################################################################
# Image processing and computer vision
#
#	Download library: opencv-python, opencv-contrib-python

import cv2
import matplotlib.pyplot as plt
import numpy


# Load and display image
img = cv2.imread('images/mount.jpg')
cv2.imshow('My window', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

# adjustable window
cv2.namedWindow('My window', cv2.WINDOW_NORMAL) # to make window resizable
cv2.imshow('My window', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

#
# Transformations
#

# scaling
height, width = img.shape[:2] # get size
scaled = cv2.resize(img,(width // 2, height // 2), interpolation=cv2.INTER_CUBIC)

# translation
P = numpy.float32([[1, 0, width/2],[0, 1, height/2]])
translated = cv2.warpAffine(img, P,(width, height))

# rotation
R = cv2.getRotationMatrix2D((width // 2, height // 2), 45, 1) # center, angle in degrees, scale
rotated = cv2.warpAffine(img, R,(width, height)) # rotation is a special case of affine transformation

# draw all in subplots
plt.figure(1)
plt.subplot(2,2,1), plt.imshow(img) # when drawing as a plot, a color transformation must be specified
plt.subplot(2,2,2), plt.imshow(cv2.cvtColor(scaled, cv2.COLOR_BGR2RGB))
plt.subplot(2,2,3), plt.imshow(cv2.cvtColor(translated, cv2.COLOR_BGR2RGB))
plt.subplot(2,2,4), plt.imshow(cv2.cvtColor(rotated, cv2.COLOR_BGR2RGB))
plt.show()

# create synthetic white image in RGB color space
width = 800
height = 600
img = numpy.full((height,width,3), 255, numpy.uint8)  # size, value, type

# add vertical horizontal and lines
step = 100
for x in range(step, width, step):
	cv2.line(img, (x,0), (x,height), (0,0,0), 2 ) # image, start, end, color, width

for y in range(step, height, step):
	cv2.line(img, (0,y), (width,y), (0,0,255), 2 )

pts = numpy.float32([[100,100], [400,500], [700,300]])
colors = [(255,0,0), (0,255,0), (255,0,255)]

# draw points, each in different color
for pt,col in zip(pts, colors):
	cv2.circle(img, (int(pt[0]), int(pt[1])), 1, col, 20)

plt.figure("Synth")
plt.subplot(1,2,1), plt.imshow(img) # we assume RGB color space - no color transformation needed

# Affine transformation - retains line parallelism

# get new positions of points
pts_new = numpy.float32([[200,200], [400,400], [500,200]])

# calculate 2x3 transformation matrix
P = cv2.getAffineTransform(pts, pts_new)

# perform transformation
img_new = cv2.warpAffine(img, P, (height, width))

plt.subplot(1,2,2), plt.imshow(img_new)

#
# Thresholding
#
img = cv2.imread('images/text.jpg')

# transform to gray scale (intensity image)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

plt.figure("threshold")
plt.subplot(2,2,1), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.subplot(2,2,1).axis('off')
plt.subplot(2,2,2), plt.imshow(gray,'gray')
plt.subplot(2,2,2).axis('off')

# Peform global thresholding - constant threshold value for the entire image
# Parameters:
# input, threshold, max value, type
ret,th1 = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

plt.subplot(2,2,3), plt.imshow(th1,'gray')
plt.subplot(2,2,3).axis('off')

# Adaptive thresholding - threshold adjusted locally (in a neighbourhood of given size)
# Parameters:
# input, max value, adaptive method, type, neighbourhood size, constant subtracted from the mean
th3 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 3)

plt.subplot(2,2,4), plt.imshow(th3,'gray')
plt.subplot(2,2,4).axis('off')
plt.show()

#
# Mathematical morphology
#
# Input: binary image A, structuring element B.
# Structuring element has a central point. We test all pixels of input image by placing the structuring element
# in it and see what pixels it covers. The result depends on the operation:
# - erosion A - B
# - dilation A + B
# - opening: erosion followed by dilation (A - B) + B
# - closing: dilation followed by erosion (A + B) - B

# extract input image and invert (white objects on black background)
A = th3[0:100,0:100]
A = (A + 1) % 2 # change 0 to 1 and opposite

plt.figure("Morphology")
plt.subplot(2,3,1), plt.imshow(A,'gray')
plt.title("Input")

# create structuring element
B = numpy.array([[0,1,0],[1,1,1],[0,1,0]], numpy.uint8)
plt.subplot(2,3,4), plt.imshow(B,'gray')
plt.title("Structuring element")

# erosion
A_erosion = cv2.erode(A, iterations=1, kernel=B)
plt.subplot(2,3,2), plt.imshow(A_erosion,'gray')
plt.title("Erosion")

# dilation
A_dilation = cv2.dilate(A, iterations=1, kernel=B)
plt.subplot(2,3,5), plt.imshow(A_dilation,'gray')
plt.title("Dilation")

# opening - erosion + dilation
A_opening = cv2.morphologyEx(A, cv2.MORPH_OPEN, kernel=B, iterations=1)
plt.subplot(2,3,3), plt.imshow(A_opening,'gray')
plt.title("Opening")

# closing - dilation + erosion
A_closing = cv2.morphologyEx(A, cv2.MORPH_CLOSE, kernel=B, iterations=1)
plt.subplot(2,3,6), plt.imshow(A_closing,'gray')
plt.title("Closing")

# remove dots from the text
plt.figure(2)
kernel = numpy.ones((2,2), numpy.uint8)
morph = cv2.morphologyEx(th3, cv2.MORPH_CLOSE, B, iterations=2) # closing instead of opening - colour inversion
morph = cv2.erode(morph, iterations=1, kernel=kernel)
plt.imshow(morph, 'gray')
plt.show()

#
# Edge detection
#
# Canny - one of the most popular algorithms. Steps:
# 1. Noise reduction with 5x5 Gaussian filter
# 2. Finding intensity image gradient - apply Sobel filters and combine results to obtain gradient value and direction
# 3. Removing points that for sure not constitute for an edge.
# 4. Thresholding edges points with hysteresis:
#  	- larger threshold for identifying initial segments,
#	- smaller threshold for line continuation.
plt.figure(3)
img = cv2.imread('images/tower.jpg')
edges = cv2.Canny(img, 100, 250) # hysteresis parameters

plt.subplot(1,2,1),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.title('Original'), plt.xticks([]), plt.yticks([])
plt.subplot(1,2,2),plt.imshow(edges,'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])

plt.show()