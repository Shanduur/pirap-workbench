import numpy as np

########################################################################################################################
# Plotting packages:
# - matplotlib - Matlab equivalent,
# - plotnine - R's ggplot equivalent (The Grammar of Graphics).

import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as plt3d

# First matplotlib plot
X = np.arange(0, 2 * np.pi, 0.005)
S,C = np.sin(X), np.cos(X*X)*np.exp(-X/2)

plt.figure()
plt.plot(X,C)
plt.plot(X,S)
plt.show()

# series properties, LaTeX label formatting
plt.figure()
plt.plot(X, S, color='red', linewidth=2, 	label=r'$\sin(x)$')
plt.plot(X, C, linestyle='--', 	label=r'$\cos(x^2)e^{\frac{-x}{2}}$')

# y axis range, x axis suited to data
plt.ylim(S.min()*1.5, S.max()*1.1)

# x axis markers
plt.xticks([0, np.pi/2, np.pi, 3*np.pi/2, 2*np.pi],
	[r'$0$', r'$\pi/2$', r'$\pi$', r'$3\pi/2$', r'$2\pi$'])

# the legend
plt.legend(loc='upper right')

# grid
plt.grid(which='major', axis='x')
plt.grid(which='both', axis='y')

# scatter plot
plt.figure("scatter plot")
# generate two clusters
X1 = np.random.randn(30) * 0.1 + 0.3
Y1 = np.random.randn(30) * 0.2 + 0.5

X2 = np.random.randn(50) * 0.05 + 0.8
Y2 = np.random.randn(50) * 0.05 + 0.8

plt.scatter(X1, Y1, color='red', marker='x', label=r'cluster 1');
plt.scatter(X2, Y2, color='blue', marker='o', label=r'cluster 2');
plt.legend(loc='lower right')
plt.show()

# 3D plot - surface
fig = plt.figure(4)
ax = plt3d.Axes3D(fig)
X = np.arange(-1, 1, 0.05)
Y = np.arange(-1, 1, 0.05)
X, Y = np.meshgrid(X, Y)
Z = np.sqrt(np.sin(X + Y)**2 + np.cos(X - Y)**2)
ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='hot')
plt.show()


# 3D plot - parametric curve
fig = plt.figure(5)
ax = fig.gca(projection='3d') # alternative axis configuration
t = np.arange(0, 15, 0.05)
r = 1
X = np.cos(t) * r
Y = np.sin(t) * r
Z = t[:] / 10
ax.plot(X, Y, Z)

plt.show()

########################################################################################################################
# Optimization
#
# Package scipy.optimize - algorithms for local and global optimization.

import scipy.optimize as opt

# lambda expression of function to be minimized
fx = lambda x: (x+6)*(x+1)*(x-2)*(x-4)

# local optimization
loc = opt.minimize_scalar(fx) 					# default variant
loc = opt.minimize_scalar(fx, method='Brent') 	# specify algorithm

# global optimization (metaheuristic which uses local optimization)

glo = opt.basinhopping(fx, 0) # default, 0 - initial guess

args = {'method': 'L-BFGS-B'} # specify local minimizer to be used
glo = opt.basinhopping(fx, 0, minimizer_kwargs=args)

print('loc=',loc.x, 'glo=',glo.x)

# plot the function
plt.figure()
X = np.arange(-7, 5, 0.1)
Y = fx(X)
plt.plot(X,Y)

# plot local minimum
plt.scatter(loc.x, fx(loc.x), color='red' )

# add text annotation
plt.annotate(
	r'local', # text to be plotted
	xy=(loc.x, fx(loc.x)),  	# annotated point coordinates
	xycoords='data', 			# use data coordinate system
	xytext=(0, -50),  			# text coordinate
	textcoords='offset points', # offset in points
	fontsize=16,
    arrowprops=dict(arrowstyle="->")) # style of arrow

# repeat for global optimum
plt.scatter(glo.x, fx(glo.x), color='blue' )
plt.annotate(r'global', xy=(glo.x, fx(glo.x)), xycoords='data', \
             xytext=(0, +50), textcoords='offset points', fontsize=16, \
             arrowprops=dict(arrowstyle="->"))

# configure axes
ax = plt.gca()

# hide top and right axis
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# move bottom and left axes
ax.xaxis.set_ticks_position('bottom')
ax.spines['bottom'].set_position(('data',0)) # no shift w.r.t data coordinate system
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data',0)) # no shift w.r.t data coordinate system
plt.show()

########################################################################################################################
# Statistical data analysis
#
# Package scipy.stats:
# - tens of discrete and continuous distributions,
# - calculating numerical characteristics of distribution,
# - statistical tests

import scipy.stats as st

# floating-point range
x = np.arange(0, 10, 0.05)

dist = st.norm(loc=5, scale=1)	# normal distribution
Fx = dist.cdf(x)				# cumulative distribution function (CDF)
fx = dist.pdf(x)				# probability density function (PDF)

# plot CDF and PDF
plt.figure(1)
plt.plot(x,Fx, color='red', label='CDF')
plt.plot(x,fx, color='blue', label='PDF')
plt.legend(loc='best')
plt.show()

# two normal distributions
N1 = st.norm(loc=4, scale=2)
N2 = st.norm(loc=3, scale=0.5)

# draw 1000-element samples from distributions
X1 = N1.rvs(size=10000)
X2 = N2.rvs(size=10000)

# visualize theoretical and empirical distributions
plt.figure(2)
plt.plot(x, N1.pdf(x), color='red', linewidth=3)
plt.hist(X1, density='True', color='red', alpha=0.5)
plt.plot(x, N2.pdf(x), color='blue', linewidth=3)
plt.hist(X2, density='True', color='blue', alpha=0.5)
plt.show()

# two similar distributions
N1 = st.norm(loc=4.0, scale=1)
N2 = st.norm(loc=4.2, scale=1)

# draw samples from both distributions and test the means
t_10, p_10 = st.ttest_ind(N1.rvs(size=10), N2.rvs(size=10)) 	# get test statistics and p-value
t_1000, p_1000 = st.ttest_ind(N1.rvs(size=1000), N2.rvs(size=1000))

print(p_10, p_1000) # explain the difference in p-values



########################################################################################################################
# Data mining
#
# SciPy contains k-means and hierarchical clustering algorithms.
# More functionalities are provided by scikit-learn package:
# - classification,
# - regression,
# - clustering,
# - dimensionality reduction.


# generate random data
sizeA = 50
sizeB = 30

distA = st.multivariate_normal(mean=(0.3, 0.5), cov=[[0.03, 0],[0, 0.03]] )
distB = st.multivariate_normal(mean=(0.8, 0.8), cov=[[0.02, 0],[0, 0.02]] )

A = distA.rvs(sizeA)
B = distB.rvs(sizeB)

# visualize
f1 = plt.figure(1)
plt.scatter(A[:,0], A[:,1], color='red', marker='x', label=r'A')
plt.scatter(B[:,0], B[:,1], color='blue', marker='o', label=r'B')
plt.legend(loc='lower right')
plt.show()

# form data matrix
A = np.hstack((A, np.zeros((sizeA, 1))))
B = np.hstack((B, np.ones((sizeB, 1))))
D = np.vstack((A, B))
np.random.shuffle(D)

# perform clustering
import scipy.cluster.vq as clust

(means, labels) = clust.kmeans2(D[:,0:2], 2) # use only attributes

# draw clustering results
f2 = plt.figure(2)
clusA = D[labels == 0, :]
clusB = D[labels == 1, :]

plt.scatter(clusA[:,0], clusA[:,1], color='red', marker='x', label=r'cluster A')
plt.scatter(clusB[:,0], clusB[:,1], color='blue', marker='o', label=r'cluster B')
plt.show()

# classification
from sklearn.ensemble import RandomForestClassifier

testA = distA.rvs(10)
testB = distB.rvs(10)
testD = np.vstack((testA, testB))

clf = RandomForestClassifier(max_depth=None, min_samples_split=2, random_state=0)
clf.fit(D[:,0:2], D[:,2])
labels = clf.predict(testD)

f3 = plt.figure(3)
plt.scatter(A[:,0], A[:,1], color='red', marker='x', label=r'train A')
plt.scatter(B[:,0], B[:,1], color='blue', marker='o', label=r'train B')
plt.scatter(testA[:,0], testA[:,1], color='black', marker='x', label=r'test A')
plt.scatter(testB[:,0], testB[:,1], color='black', marker='o', label=r'test B')

plt.scatter(testD[labels == 0,0], testD[labels == 0,1], edgecolors='red', marker='o', s=100, facecolors='none', label=r'predict A')
plt.scatter(testD[labels == 1,0], testD[labels == 1,1], edgecolors='blue', marker='o', s=100, facecolors='none', label = r'predict B')

plt.legend(loc='lower right')
plt.show()

# cross-validation
from sklearn.model_selection import cross_val_score
from sklearn.datasets import make_blobs
from sklearn.naive_bayes import GaussianNB

# generate random data
X, y = make_blobs(n_samples=10000, n_features=10, centers=100, random_state=0)
rf = RandomForestClassifier(max_depth=None, min_samples_split=2,random_state=0)
scores = cross_val_score(rf, X, y, cv=5)
scores.mean()

nb = GaussianNB()
scores = cross_val_score(nb, X, y, cv=5)
scores.mean()