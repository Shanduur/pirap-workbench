# auxiliary function for cleaning the workspace
def clear_all():
    gl = globals().copy()
    for var in gl:
        if var[0] == '_': continue
        if 'func' in str(globals()[var]): continue
        if 'module' in str(globals()[var]): continue

        del globals()[var]

# --------------------------------------------------------------------
# Variables initialization
#
# - declared during initialization,
# - automatic type deduction.
clear_all()

i=10 # int
x=3.14 # float (64-bit)
s1='Hello' # str
s2="I'm John" # str
b=True # bool
type(i)

x = 'abc' # reassignment with different type

exp = 100 # int
y = i ** exp # power operator, auto extension (infinite range)
type(y) # still int

# --------------------------------------------------------------------
# Operators

# Arithmetic:       + - * / % // **
# Logical:          and or not
# Bitwise:          << >> & | ~ ^
# Relation:         < > <= >= == !=
# Assignment:       = += -= *= /= %= **=
# Special:          in, not in, is, is not
clear_all()
i = 10
x = 40.0
s = 'abc'

print(i/4)	    # 2.5 - floating-point division (from Python 3)
print(i//4)	    # 2 - integer division (from Python 3)
print(i/4.0) 	# 2.5
print(i,x,s) 	# 10 40.0 abc
print(s+'xyz') 	# abcxyz - string concatenation
print(s*2) 	    # abcabc - string multiplication
print(isinstance(i, int)) # True
print(type(s*3) == bool)  # False
q = int(3.7) 	# narrowing conversion
j = int('123') 	# conversion
k = int('123.55') # ERROR

# --------------------------------------------------------------------
# Code organization
#
# Blocks defined by tab
id = 0
if id > 0:
    x = 1
    y = 1
elif id == 0:
    x = 2; y = 2
else: 
    x = 3

# --------------------------------------------------------------------
# Input and output
clear_all()
age = 44
name = 'Peter'
avg = 4.333333

# formatted output
a = f'{name} is {age} years old and has an average grade of {avg:.2f}'               # formatted string literals
b = '{} is {} years old and has an average grade of {:.2f}'.format(name, age, avg)   # using format() method
c = '%s is %d years old and has an average grade of %.2f' % (name, age, avg)         # old way: using %


# raw input
str1 = input('Gimme string! ')

# input with conversion
age = eval(input('How old are you? ')) # evaluates argument as if it was Python code - unsafe
if isinstance(age, int):
    print('You are ', age)
else:
    print('Wrong type')

# using exceptions
try:
    i = int(input('Gimme int '))
    y = float(input('Gimme float '))
except ValueError:
	print('Parsing error')

# --------------------------------------------------------------------
# Importing modules

# load modules
import math 	# system module
#import mylib 	# own module
y = math.sin(math.pi)
#z = mylib.myfun()

# import single element
from math import pi
y = math.sin(pi)

# import everything
from math import * # bad idea
y = sin(pi)

# alias
import math as m
y = m.sin(m.pi)

# --------------------------------------------------------------------
# Functions

# no return value
def display(x):
    print(x)

# return value
def sqr(x):
   return x * x

# function that does nothing
def doNothing():
    pass

# conditional return
def geoMean(x : float, y : float): # optional type specification for syntax hints
    if x < 0 or y < 0: return None
    else: return math.sqrt(x * y)

# b = display('abc') #  b is None
p = sqr(2) # p is 4
q = geoMean(2, 8) # q is 4
r = geoMean(-2, 8) # r is None

# default and named arguments
def fun(x, y=10, s='abc'):
    print(x,y,s)

fun(0) 		            # fun(0, 10, 'abc')
fun(1, 3.14, 'xyz')
fun(2, s='PS') 	        # named argument - fun(2, 10, 'PS')
fun(y=4, x=1)	        # named arguments - fun(1, 4, 'abc')
# fun(5, x=1)             # error: x passed twice

# def fun2(x=1, y): # error: non-default follows default
#     pass

# --------------------------------------------------------------------
# Iterations

# Iterate over collection
V = ['a', 'b', 'c'] # list comprehension - creates a list from values
for e in V:
    print(e)

# Iterate over range (right excluded)
for i in range(0,10): #  [0, 1, ... 9]
    print(i)

# while loop
i = 10
while i > 0:
    print(i)
    i -= 1

# 100, 95, ...,5
for i in range(100, 0, -5): print(i)

# reversed collection
for e in reversed(V):
    print(e)

# break, continue, loop-else
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(f'{n} = {x} * {x}')
            break
    else: # concerns for loop - executed when loop exited normally (not by break)
        print ('{} is a prime number'.format(n))



