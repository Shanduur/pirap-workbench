"""
LAB 3
"""

'''
Task 1
'''
class StatePool:
    def __init__(self):
        self.__data = {}

    def GetInst(self, cls: any) -> any:
        if cls not in self.__data:
            self.__data[cls] = cls()

        return self.__data[cls]


'''
Task 2
'''
class HeroState:
    __spl = StatePool()

    @classmethod
    def GetInst(cls):
        return HeroState.__spl.GetInst(cls)


'''
Task 3
'''
class Standing(HeroState):
    def __str__(self) -> str:
        return 'Hero is standing'

    def on_event(self, event: str) -> HeroState:
        if event == 'go':
            return Moving.GetInst()
        elif event == 'shoot':
            state = Shooting.GetInst()
            state._prev = self
            return state
        else:
            return self


class Moving(HeroState):
    def __str__(self) -> str:
        return 'Hero is moving'

    def on_event(self, event: str) -> HeroState:
        if event == 'stop':
            return Standing.GetInst()
        elif event == 'shoot':
            state = Shooting.GetInst()
            state._prev = self
            return state
        else:
            return self


class Shooting(HeroState):
    def __str__(self) -> str:
        return 'Hero is shooting'

    def on_event(self, event: str) -> HeroState:
        return self._prev


'''
Task 4
'''
class Hero:
    state = Standing()

    def ProcessEvents(self):
        while True:
            print(self.state)
            event = input('Action: ')
            if event == 'exit':
                return
            else:
                self.state = self.state.on_event(event)


def main():
    player = Hero()

    player.ProcessEvents()
    pass


if __name__ == "__main__":
    main()
