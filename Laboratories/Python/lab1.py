"""
Task 1
"""


def findPositions(string: str, *keys) -> dict:
    d = dict()

    for key in keys:
        val = [i for i, x in enumerate(string) if x == key]
        if val:  # check if list is non empty
            d[key] = val

    return d


"""
Task 2
"""


def positionsToTupleList(positions: dict) -> list:
    l = list()

    for key in positions:
        for val in positions[key]:
            l.append((val, key))

    return sorted(l)


def positionsToTupleList_comprehension(positions: dict) -> list:
    l = [(val, key) for key in positions for val in positions[key]]

    return sorted(l)


"""
Task 3
"""


def dictSortedGenerator(d: dict):
    # create dictionary from sorted items of original dict
    # where we are sorting by element 0 - key,
    # instead of value - element 1
    s = dict(sorted(d.items(), key=lambda item: item[0]))

    for key in s:
        yield key, s[key]


"""
Task 4
"""


def drawHisto(d: dict) -> None:
    d2 = {key: len(val) for key, val in d.items()}

    for key, val in dictSortedGenerator(d2):
        print(key, "*" * val)


"""
Task 5
"""


def convertToUnique(l: list) -> list:
    return list(set(l))


"""
Task 6
"""


def getJaccardIndex(set1: set, set2: set) -> float:
    # | set1 intersects set2 | / | set1 union set2 |
    ins = len(set1 & set2)
    uni = len(set1 | set2)

    return ins / uni


"""
Task 7
"""


def optimizeJaccardIndex(set1: set, set2: set, limit=0.9) -> float:
    index = getJaccardIndex(set1, set2)

    # checking if set1 and set2 have any values
    while index < limit and set1 and set2:
        try:
            if len(set1) < len(set2):
                tmp = (set2 - set1).pop()
                set2.remove(tmp)
            else:
                tmp = (set1 - set2).pop()
                set1.remove(tmp)
        except KeyError:
            break

        index = getJaccardIndex(set1, set2)

    return index


def main():
    pos = findPositions("test" * 2 + "xD", "a", "e", "s", "x")
    print('\nTASK 1:\n', pos)

    tlist1 = positionsToTupleList(pos)
    print('\nTASK 2 TL:\n', tlist1)
    tlist2 = positionsToTupleList_comprehension(pos)
    print('\nTASK 2 TLC:\n', tlist2)

    print('\nTASK 3:')
    for a, b in dictSortedGenerator({5: 1, 1: 5}):
        print(f"k={a}, v={b}")

    print('\nTASK 4:')
    drawHisto(pos)

    print('\nTASK 5:\n', convertToUnique([1, 2, 1, 2, 6, 7, 6, 9, 9, 9, 10]))

    s1 = {0, 5, 4}
    s2 = {5, 2, 4}

    print('\nTASK 6:')
    print(f's1: {s1}\ns2: {s2}\nJI: {getJaccardIndex(s1, s2)}')

    print('\nTASK 7:')
    index = optimizeJaccardIndex(s1, s2)
    print(f's1: {s1}\ns2: {s2}\nJI: {index}')

    print('\n')  # for visibility


if __name__ == '__main__':
    main()
