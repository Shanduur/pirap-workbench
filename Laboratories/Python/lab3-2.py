"""
LAB 3
"""
import multiprocessing as mp
import numpy as np
import time
import math as m


'''
Task 5
'''
def root(number: float, order: int) -> float:
    return number ** (1.0 / (order+2))


def sequence(x: float, results: np.array, iterations: int) -> None:
    for i in range(iterations):
        results[i] = root(x, i)


'''
Task 6
'''
def parallel(x: float, results: np.array, iterations: int, pool: mp.Pool) -> None:
    with pool:
        res = pool.starmap(root, ((x, i) for i in range(iterations)))
    results[:] = res


'''
Task 7
'''
def chunkRoots(number: float, begin: int, size: int) -> np.array:
    results = np.empty(size)
    for i in range(size):
        results[i] = root(number, begin + i)
    return results


def parallelChunks(x: float, results: np.array, size: int, pool: mp.Pool, workers: int) -> None:
    with pool:
        res = pool.starmap(chunkRoots, ((x, i * size, size) for i in range(workers)))
    results[:] = np.concatenate(res)


'''
Task 8
'''
def task(job_id: int, lock: mp.Lock) -> None:
    lock.acquire()
    try:
        print("Job id: %d" % job_id)
    finally:
        lock.release()


def main():
    threads = mp.cpu_count()
    iterations = m.ceil(int(1e6) / threads) * threads
    # iterations = int(1e6)
    chunkSize = iterations // threads

    x = np.random.uniform(1, 100)
    results = np.empty(iterations)

    times = []

    print(f'\nTASK 5:\nIterations: {iterations}')

    start = time.perf_counter()
    sequence(x, results, iterations)
    stop = time.perf_counter()
    times.append(stop - start)
    print('Calc Time: ', times[-1])

    pool = mp.Pool(threads)
    print(f'\nTASK 6:\nIterations: {iterations}\nThreads: {threads}')

    start = time.perf_counter()
    parallel(x, results, iterations, pool)
    stop = time.perf_counter()
    times.append(stop - start)
    print('Calc Time: ', times[-1])

    pool2 = mp.Pool(threads)
    print(f'\nTASK 7:\nIterations: {iterations}\nThreads: {threads}')

    start = time.perf_counter()
    parallelChunks(x, results, chunkSize, pool2, threads)
    stop = time.perf_counter()
    times.append(stop - start)
    print('Calc Time: ', times[-1])

    print('\n-----\nBaseline: ', times[0])
    for t in times[1:]:
        print(f'Result: {t}\tSpeedup: {times[0]/t}')
    print('-----')

    print('\nTASK 8:')
    lock = mp.Lock()
    workers = [mp.Process(target=task, args=(job, lock)) for job in range(10)]
    for worker in workers:
        worker.start()
    for worker in workers:
        worker.join()


if __name__ == "__main__":
    main()
