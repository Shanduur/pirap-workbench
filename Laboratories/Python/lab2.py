"""
LAB 2
"""
import numpy as np
import random

'''
Task 1
'''
def getDistanceMatrix(arr: np.array) -> np.array:
    n = arr.shape[0]
    out = np.zeros((n, n))

    # SLOWER ?
    # for x, y in np.ndindex(out.shape):
    #     out[x, y] = np.linalg.norm(arr[x] - arr[y])

    # FASTER ?
    for x in range(0, n):
        for y in range(x, n):
            out[y, x] = out[x, y] = np.linalg.norm(arr[x] - arr[y])

    return out


'''
Task 2
'''
def eliminateDistances(f: float, Q: np.array) -> np.array:
    out = Q.copy()
    i = np.random.uniform(0, 1, Q.shape)

    out[f >= i] = np.inf

    return out


'''
Task 3
'''
def calculateFloydWarshall(Q: np.array) -> np.array:
    n = Q.shape[0]
    out = Q.copy()

    for k in range(0, n):
        for i in range(0, n):
            for j in range(0, n):
                # if Q(i,j) > Q(i,k) + Q(k,j) then Q(i,j) = Q(i,k) + Q(k,j)
                if out[i, j] > out[i, k] + out[k, j]:
                    out[i, j] = out[i, k] + out[k, j]

    return out


'''
Task 4
'''
def runAll(probability: float, input: np.array) ->  float:
    return float(np.sum(
        calculateFloydWarshall(
            eliminateDistances(
                probability,
                getDistanceMatrix(input)
            ))))


'''
Task 5
'''
def generateSystem(input: np.array) -> tuple:
    n = input.shape[0]

    coeff = np.random.uniform(0, 100, (n, n))
    np.fill_diagonal(coeff, 0)  # set diagonal to zeros
    non_diag = np.sum(abs(coeff), axis=1)
    np.fill_diagonal(coeff, non_diag)

    return coeff, coeff @ input


'''
Task 6
'''
def solveSystem(coeffs: np.array, free_terms: np.array,
                convergence=0.01, max_iterations=1000) -> np.array:
    x = np.zeros((coeffs.shape[0], 1))
    diag = np.array([np.diag(coeffs)]).T
    for i in range(0, max_iterations):
        prev_x = x.copy()
        x = (free_terms - ((coeffs - np.diagflat(diag)) @ x)) / diag
        if np.allclose(x, prev_x, convergence):
            break

    return x

'''
Task 7
'''
import time


def benchmark(func: callable, args) -> None:
    start = time.perf_counter()
    res = func(*args)
    stop = time.perf_counter()
    print(f'FUNC: {func.__name__}\nEXEC TIME:{(stop-start)*1000}s\nRESULT:\n {res}\n')


def invDot(coeffs: np.array, free_terms: np.array):
    return np.linalg.inv(coeffs).dot(free_terms)


def solve(coeffs: np.array, free_terms: np.array):
    return np.linalg.solve(coeffs, free_terms)


def main():
    arr = np.array([
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
        [10, 11, 12]
    ])
    dist = getDistanceMatrix(arr)
    print('\nTASK 1:\n', dist)

    p = random.random()
    print(f'\nTASK 2:\nPROBABILITY: {p}\nRESULT:\n', eliminateDistances(p, arr.astype(float)))

    arr = np.array([
        [10, 1, 10],
        [3, 4, 5],
        [6, 7, 8]
    ])
    print('\nTASK 3:\n', calculateFloydWarshall(arr))

    print('\nTASK 4:')
    randSet = np.random.uniform(0, 100, (100, 3))
    print(np.sum(getDistanceMatrix(randSet)))
    for i in [0.5, 0.9, 0.99]:
        print(f'PROBABILITY: {i}\nRESULT: ', runAll(i, randSet))
    ''' 
    it looks like the results for probability 0.5 and 0.9 are calculated,
    but for probability 0.99 it is impossible to reduce infinities, 
    and the final result is equal to inf
    '''

    input = np.array([np.arange(3)]).T
    linear_system = generateSystem(input)
    print(f'\nTASK 5:\nINPUT:\n{input}')
    print(f'COEFFICIENTS:\n{linear_system[0]}\nFREE TERMS:\n{linear_system[1]}')

    solution = solveSystem(*linear_system)
    print('\nTASK 6:')
    print('SOLUTION\n', solution)
    print('NP.LINALG.SOLVE:\n', np.linalg.solve(*linear_system))
    print('NP.LINALG.INV.DOT:\n', np.linalg.inv(linear_system[0]).dot(linear_system[1]))

    print('\nTASK 7:\n')
    benchmark(invDot, linear_system)
    benchmark(solve, linear_system)
    benchmark(solveSystem, linear_system)

    pass


if __name__ == '__main__':
    main()
