library(shiny)
library(ISLR)
library(knitr)
library(gridExtra)
library(ggplot2)

shinyUI(fluidPage(

    titlePanel("Report"),

    sidebarLayout(
        sidebarPanel(
            selectInput("stat",
                        "Choose the stat summary",
                        c("CAtBat",
                          "CHits",
                          "CHmRun",
                          "CRuns",
                          "CRBI",
                          "CWalks")
            ),
            
            selectInput("league",
                        "Choose the League",
                        levels(Hitters$League)
            )
        ),

        mainPanel(
            tabsetPanel(
                tabPanel("Summary",
                         verbatimTextOutput("summary")),
                tabPanel("Plot",
                         plotOutput("splot")),
                tabPanel("Players",
                         tableOutput("table")),
                tabPanel("Salaries",
                         tableOutput("salaries"))
                
            )
        )
    )
))
